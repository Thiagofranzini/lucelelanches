package br.com.lucelelanches.model.bo.ingredientes.impl;

import static br.com.lucelelanches.util.GerarMassaUtil.criarListaPaes;
import static br.com.lucelelanches.util.GerarMassaUtil.criarListaQueijos;
import static br.com.lucelelanches.util.GerarMassaUtil.criarListaRecheios;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.lucelelanches.model.bo.ingredientes.IngredientesBO;
import br.com.lucelelanches.model.entity.Pao;
import br.com.lucelelanches.model.entity.Queijo;
import br.com.lucelelanches.model.entity.Recheio;

/**
* 
* @author thiago.franzini
*
*/
@Service
public class IngredientesBOImpl implements IngredientesBO {

	private static final long serialVersionUID = 3164374670424704991L;

	@Override
	public List<Queijo> listarQueijos() {
		return criarListaQueijos();
	}

	@Override
	public List<Pao> listarPaes() {
		return criarListaPaes();
	}
	
	@Override
	public List<Recheio> listarRecheios() {
		return criarListaRecheios();
	}

}
