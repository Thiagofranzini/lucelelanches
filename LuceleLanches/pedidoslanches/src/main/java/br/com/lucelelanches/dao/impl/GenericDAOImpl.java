package br.com.lucelelanches.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import br.com.lucelelanches.dao.GenericDAO;
import br.com.lucelelanches.dao.util.HibernateUtil;

/**
 * Classe DAO Genérica para utilização nas consultas.
 * @author thiago.franzini
 *
 */
@Repository
public class GenericDAOImpl<E> implements GenericDAO<E> {

	private static final String FROM = "from ";
	private static final String DELETE_FROM = "delete from ";
	private static final String WHERE_ID = " where id = ";
	
	private static final long serialVersionUID = -6221806237112988036L;
	
	private EntityManager entityManager;
	
	public GenericDAOImpl(){
		this.entityManager = HibernateUtil.getOpenConnection();
	}
	
	
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public void save(E entidade){
		try {
			this.getEntityManager().persist(entidade);
		} catch (Exception e) {
			this.getEntityManager().getTransaction().rollback();
			e.printStackTrace();
		}
	}
	
	@Override
	public void update(E entidade){
		this.getEntityManager().merge(entidade);
	}
	
	@Override
	public void delete(@SuppressWarnings("rawtypes") Class classe, Integer id){
		this.getEntityManager().createQuery(DELETE_FROM+ classe.getName() + WHERE_ID + id).executeUpdate();
	} 
	
	@Override
	public List<E> getAll(@SuppressWarnings("rawtypes") Class classe){
		List<E> lista = new ArrayList<E>();
		try {
			lista = recuperarObjetos(classe);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public E findById(E entidade, Integer id){
		return (E) this.getEntityManager().find(entidade.getClass(), id);
	}
	
	@SuppressWarnings("unchecked")
	private List<E> recuperarObjetos(@SuppressWarnings("rawtypes") Class classe) {
		List<E> lista;
		lista = (List<E>) this.getEntityManager().createQuery(FROM + classe.getName()).getResultList();
		return lista;
}
}
