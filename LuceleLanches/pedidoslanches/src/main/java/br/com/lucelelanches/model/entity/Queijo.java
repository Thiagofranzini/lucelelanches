package br.com.lucelelanches.model.entity;

import java.math.BigDecimal;
/**
 * Entidade que representa um dos ingredientes do Lanche.
 * @author thiago.franzini
 *
 */
public class Queijo {

	private Integer id;
	private String nomeQueijo;
	private BigDecimal valor;

	public String getNomeQueijo() {
		return nomeQueijo;
	}

	public void setNomeQueijo(String nomeQueijo) {
		this.nomeQueijo = nomeQueijo;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
