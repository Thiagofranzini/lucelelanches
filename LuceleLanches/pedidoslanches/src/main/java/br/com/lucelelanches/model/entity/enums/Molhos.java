package br.com.lucelelanches.model.entity.enums;

import java.util.Arrays;
import java.util.List;

/**
 * Classe Enum que representa um dos ingredientes do Lanche.
 * @author thiago.franzini
 *
 */
public enum Molhos {

	ITALIANO(1, "italian","Italiano"),
	APIMENTADO(2,"spicy", "Aplimentado");
	
	private int chave;
	private String label;
	private String valor;
	
	private Molhos(int chave, String label, String valor){
		this.chave = chave;
		this.valor = valor;
		this.label = label;
	}
	
	public String getLabel(){
		return this.label;
	}
	
	public int getChave(){
		return this.chave;
	}
	
	public String getValor(){
		return this.valor;
	}
	
	public static List<Molhos> listarMolhos(){
		return Arrays.asList(Molhos.values());
	}
	
}
