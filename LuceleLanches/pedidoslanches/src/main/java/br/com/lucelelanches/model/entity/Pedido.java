package br.com.lucelelanches.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Entidade que representa o pedido finalizado.
 * @author thiago.franzini
 *
 */
public class Pedido implements Serializable {

	private static final long serialVersionUID = -2360956230268481135L;
	private Integer id;
	private String nomeUsuario;
	private String endereco;
	private List<Lanche> lanchesPedido;
	private BigDecimal valorPedido;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public List<Lanche> getLanchesPedido() {
		return lanchesPedido;
	}

	public void setLanchesPedido(List<Lanche> lanchesPedido) {
		this.lanchesPedido = lanchesPedido;
	}

	public BigDecimal getValorPedido() {
		return valorPedido;
	}

	public void setValorPedido(BigDecimal valorPedido) {
		this.valorPedido = valorPedido;
	}

}
