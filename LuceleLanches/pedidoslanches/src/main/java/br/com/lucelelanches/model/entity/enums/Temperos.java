package br.com.lucelelanches.model.entity.enums;

import java.util.Arrays;
import java.util.List;

/**
 * Classe Enum que representa um dos ingredientes do Lanche.
 * @author thiago.franzini
 *
 */
public enum Temperos {

	PIMENTA(1, "chili","Pimenta"),
	SAL(2, "salt", "Sal"),
	OREGANO(3, "oregano", "Orégano");
	
	private int chave;
	private String label;
	private String valor;
	
	private Temperos(int chave, String label,  String valor){
		this.chave = chave;
		this.valor = valor;
		this.label = label;
	}
	
	public String getLabel(){
		return this.label;
	}
	
	public int getChave(){
		return this.chave;
	}
	
	public String getValor(){
		return this.valor;
	}
	
	public static List<Temperos> listarTemperos(){
		return Arrays.asList(values());
	}
	
}
