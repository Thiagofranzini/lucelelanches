package br.com.lucelelanches.model.bo.lanches.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.lucelelanches.model.bo.lanches.LanchesBO;
import br.com.lucelelanches.model.entity.Lanche;
import br.com.lucelelanches.util.GerarMassaUtil;

@Service
public class LanchesBOImpl implements LanchesBO {

	private static final long serialVersionUID = 2182592234199629759L;

	@Override
	public List<Lanche> listarLanches() {
		return GerarMassaUtil.gerarListaLanches();
	}

}
