package br.com.lucelelanches.model.entity;

import java.io.Serializable;
/**
 * Entidade Usuario, utilizada para realizar o Login.
 * @author thiago.franzini
 *
 */
public class Usuario implements Serializable {

	private static final long serialVersionUID = 4973554416811863527L;

	private String nome;
	private String senha;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
