package br.com.lucelelanches.model.bo.lanches;

import java.io.Serializable;
import java.util.List;

import br.com.lucelelanches.model.entity.Lanche;
/**
 * Camada de BO referente ao Objeto Lanche
 * @author thiago.franzini
 *
 */
public interface LanchesBO extends Serializable {

	List<Lanche> listarLanches();
	
}
