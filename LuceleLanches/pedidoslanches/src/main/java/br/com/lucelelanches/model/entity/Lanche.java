package br.com.lucelelanches.model.entity;

import java.io.Serializable;
import java.util.List;

import br.com.lucelelanches.model.entity.enums.Molhos;
import br.com.lucelelanches.model.entity.enums.Salada;
import br.com.lucelelanches.model.entity.enums.Temperos;

/**
 * Entidade que representa um Lanche com os ingredientes adicionados pelo usuario. 
 * @author thiago.franzini
 *
 */
public class Lanche implements Serializable {

	private static final long serialVersionUID = -3326949418031130967L;
	
	private Integer id;
	private String nomeLanche;
	private Pao pao;
	private Queijo queijo;
	private Salada salada;
	private Recheio recheio;
	private List<Molhos> listaMolhos;
	private List<Temperos> listaTemperos;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Pao getPao() {
		return pao;
	}

	public void setPao(Pao pao) {
		this.pao = pao;
	}

	public Queijo getQueijo() {
		return queijo;
	}

	public void setQueijo(Queijo queijo) {
		this.queijo = queijo;
	}

	public Salada getSalada() {
		return salada;
	}

	public void setSalada(Salada salada) {
		this.salada = salada;
	}

	public List<Temperos> getListaTemperos() {
		return listaTemperos;
	}

	public void setListaTemperos(List<Temperos> listaTemperos) {
		this.listaTemperos = listaTemperos;
	}

	public List<Molhos> getListaMolhos() {
		return listaMolhos;
	}

	public void setListaMolhos(List<Molhos> listaMolhos) {
		this.listaMolhos = listaMolhos;
	}

	public Recheio getRecheio() {
		return recheio;
	}

	public void setRecheio(Recheio recheio) {
		this.recheio = recheio;
	}

	public String getNomeLanche() {
		return nomeLanche;
	}

	public void setNomeLanche(String nomeLanche) {
		this.nomeLanche = nomeLanche;
	}

}
