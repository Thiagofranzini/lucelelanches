package br.com.lucelelanches.model.bo.ingredientes;

import java.io.Serializable;
import java.util.List;

import br.com.lucelelanches.model.entity.Pao;
import br.com.lucelelanches.model.entity.Queijo;
import br.com.lucelelanches.model.entity.Recheio;

/**
* Camada de BO referente aos Ingredientes
* @author thiago.franzini
*
*/
public interface IngredientesBO extends Serializable {

	List<Queijo> listarQueijos();
	List<Pao> listarPaes();
	List<Recheio> listarRecheios();
	
}
