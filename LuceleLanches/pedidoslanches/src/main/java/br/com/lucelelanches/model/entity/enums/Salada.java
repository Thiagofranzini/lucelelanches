package br.com.lucelelanches.model.entity.enums;

import java.util.Arrays;
import java.util.List;

/**
 * Classe Enum que representa um dos ingredientes do Lanche.
 * @author thiago.franzini
 *
 */
public enum Salada {

	ALFACE(1,"lettuce","Alface"),
	RUCULA(2,"arugula", "Rúcula"),
	ACELGA(3, "chard", "Acelga");
	
	private int chave;
	private String label;
	private String valor;
	
	private Salada(int chave, String label, String valor){
		this.chave = chave;
		this.label = label;
		this.valor = valor;
	}
	
	public int getChave(){
		return this.chave;
	}
	
	public String getLabel(){
		return this.label;
	}
	
	public String getValor(){
		return this.valor;
	}
	
	public static List<Salada> listarSaladas(){
		return Arrays.asList(values());
	}
}
