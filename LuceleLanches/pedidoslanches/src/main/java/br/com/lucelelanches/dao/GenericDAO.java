package br.com.lucelelanches.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<E> extends Serializable {

	void save(E entidade);

	void update(E entidade);

	void delete(@SuppressWarnings("rawtypes") Class classe, Integer id);

	List<E> getAll(@SuppressWarnings("rawtypes") Class classe);

	E findById(E entidade, Integer id);

}
