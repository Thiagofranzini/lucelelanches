package br.com.lucelelanches;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Configuração e busca das classes que serão Injetadas pelo Spring.
 * @author thiago.franzini
 *
 */

@Configuration
@ComponentScan
public class SpringConfiguration {

	
}
