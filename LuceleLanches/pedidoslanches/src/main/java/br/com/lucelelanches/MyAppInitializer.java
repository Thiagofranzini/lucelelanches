package br.com.lucelelanches;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;


/**
 * Configuração e inicialização contexto Spring.
 * @author thiago.franzini
 *
 */
public class MyAppInitializer implements WebApplicationInitializer{

	@Override
	public void onStartup(ServletContext context) throws ServletException {

		AnnotationConfigWebApplicationContext contextRoot = new AnnotationConfigWebApplicationContext();
		contextRoot.register(SpringConfiguration.class);
		
		context.addListener(new ContextLoaderListener(contextRoot));
		
	}

}
