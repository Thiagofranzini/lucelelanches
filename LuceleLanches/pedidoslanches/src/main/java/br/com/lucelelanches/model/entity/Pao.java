package br.com.lucelelanches.model.entity;

import java.math.BigDecimal;
/**
 * Entidade que representa um dos ingredientes do Lanche.
 * @author thiago.franzini
 *
 */
public class Pao {

	private Integer id;
	private String nomePao;
	private BigDecimal valor;

	public String getNomePao() {
		return nomePao;
	}

	public void setNomePao(String nomePao) {
		this.nomePao = nomePao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
