package br.com.lucelelanches.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.lucelelanches.model.entity.Lanche;
import br.com.lucelelanches.model.entity.Pao;
import br.com.lucelelanches.model.entity.Queijo;
import br.com.lucelelanches.model.entity.Recheio;
import br.com.lucelelanches.model.entity.enums.Molhos;
import br.com.lucelelanches.model.entity.enums.Salada;
import br.com.lucelelanches.model.entity.enums.Temperos;

public class GerarMassaUtil {

	private GerarMassaUtil(){}
	
	public static Queijo criarQueijo(Integer id, String nome, BigDecimal valor){
		Queijo queijo = new Queijo();
		queijo.setId(id);
		queijo.setNomeQueijo(nome);
		queijo.setValor(valor); 
		return queijo;
	}
	
	public static Pao criarPao(Integer id, String nomePao, BigDecimal valor){
		Pao pao = new Pao();
		pao.setId(id);
		pao.setNomePao(nomePao);
		pao.setValor(valor);
		return pao;
	}
	
	public static Recheio popularRecheio(Integer id, String tipoRecheio, BigDecimal valor){
		Recheio recheio = new Recheio();
		recheio.setId(id);
		recheio.setTipoRecheio(tipoRecheio);
		recheio.setValorRecheio(valor);
		return recheio;
	}
	
	public static List<Queijo> criarListaQueijos(){
		List<Queijo> queijos = new ArrayList<>();
		queijos.add(criarQueijo(1, "Parmesao", new BigDecimal("20")));
		queijos.add(criarQueijo(2, "Mussarela", new BigDecimal("20")));
		queijos.add(criarQueijo(3, "Prato", new BigDecimal("20")));
		return queijos;
	}
	
	public static List<Pao> criarListaPaes(){
		List<Pao> paes = new ArrayList<>();
		paes.add(criarPao(1, "Frances", new BigDecimal("4.50")));
		paes.add(criarPao(2, "Integral", new BigDecimal("4.50")));
		paes.add(criarPao(3, "Quatro-Queijos", new BigDecimal("4.50")));
		return paes;
	}
	
	public static List<Recheio> criarListaRecheios(){
		List<Recheio> listaRecheios = new ArrayList<>();
		listaRecheios.add(popularRecheio(1, "Barbecue", new BigDecimal("0.50")));
		listaRecheios.add(popularRecheio(1, "Teste1", new BigDecimal("0.50")));
		listaRecheios.add(popularRecheio(1, "Teste2", new BigDecimal("0.50")));
		return listaRecheios;
	}
	
	public static Lanche criarLanche(){
		Lanche lanche = new Lanche();
		lanche.setId(1);
		lanche.setNomeLanche("1 - Lanche Padrao");
		lanche.setPao(criarPao(1, "Frances", new BigDecimal("4.50")));
		lanche.setQueijo(criarQueijo(2, "Mussarela", new BigDecimal("20")));
		lanche.setRecheio(popularRecheio(1, "Barbecue", new BigDecimal("0.50")));
		lanche.setSalada(Salada.ALFACE);
		lanche.setListaMolhos(Arrays.asList(new Molhos[]{Molhos.APIMENTADO}));
		lanche.setListaTemperos(Arrays.asList(new Temperos[]{ Temperos.SAL}));
		return lanche;
	}
	
	public static Lanche criarLanche1(){
		Lanche lanche = new Lanche();
		lanche.setId(1);
		lanche.setNomeLanche("2 - Lanche padrao 2");
		lanche.setPao(criarPao(1, "Integral", new BigDecimal("4.50")));
		lanche.setQueijo(criarQueijo(2, "Mussarela", new BigDecimal("20")));
		lanche.setRecheio(popularRecheio(1, "Barbecue", new BigDecimal("0.50")));
		lanche.setSalada(Salada.RUCULA);
		lanche.setListaMolhos(Arrays.asList(new Molhos[]{Molhos.ITALIANO}));
		lanche.setListaTemperos(Arrays.asList(new Temperos[]{ Temperos.SAL, Temperos.OREGANO, Temperos.PIMENTA}));
		return lanche;
	}
	
	public static List<Lanche> gerarListaLanches(){
		List<Lanche> lanches = new ArrayList<>();
		lanches.add(criarLanche());
		lanches.add(criarLanche1());
		return lanches;
	}
	
}
