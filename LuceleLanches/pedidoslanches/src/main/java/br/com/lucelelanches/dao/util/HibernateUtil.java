package br.com.lucelelanches.dao.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateUtil {

	public static EntityManager getOpenConnection(){
		EntityManagerFactory entity = Persistence.createEntityManagerFactory("pedidoslanches");
		return entity.createEntityManager();
	}
	
}
