package br.com.lucelelanches.controller;

import java.util.Locale;

import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

/**
 * ManagedBean Utilizado para o controle de Internacionalização. 
 * @author thiago.franzini
 *
 */
public class LocaleController {

	private Locale currentLocale = new Locale("pt", "BR");

	public void englishLocale() {
		UIViewRoot vRoot = FacesContext.getCurrentInstance().getViewRoot();
		currentLocale = Locale.US;
		vRoot.setLocale(currentLocale);
	}

	public void portugueseLocale() {
		UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
		currentLocale = new Locale("pt", "BR");
		viewRoot.setLocale(currentLocale);
	}

	public Locale getCurrentLocale() {
		return currentLocale;
	}

}
