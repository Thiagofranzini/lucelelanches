# LuceneLanches #

Implementação parcial do Projeto pedidos de lanches.

Arquitetura do Projeto contem: 
Java 8;
Maven, para gerenciamento de dependencia;
Hibernate, como framework de persistencia de dados;
JSF com PrimeFaces como framework de Front end;
Spring como framework de Injeção de Dependencias;
Spring-Security utilizado para login.


Seria Implementado nas camadas de BO, toda a lógica referente as regras de negocio do projeto, como os calculos referentes a divisão dos valores dos lanches, e construção dos objetos para consulta e persistencia de dados.

Seria realizada todo o mapeamento das entidades, e a implementação da camada de DAO, responsavel por toda a lógica de persistencia do projeto.

Seria realizada a implementação dos Controllers("ManagedBeans"), para o controle e redirecionamento referente aos componentes de FrontEnd.


Para a realização do Login, com utilização do Spring-Security, seria relizada a consulta na base de dados login e senha do Usuario, que estaria ativo durente aquela sessão.

A configuração do Spring foi realizada de forma programática, com o objetivo de evitar configurações xml.


A escolha dos frameworks citados, foi realizada em busca de produtividade, simplicidade e robustez do projeto.


 
